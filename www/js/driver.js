// Constants
const SYMBOL = 'ITNS';
const FETCH_TIMEOUT_COINCAP = 20000; // 20 sec
const FETCH_TIMEOUT_HASHWHALE = 5000; // 5 sec

// Plots
const PlotCoincap = new SuperPlot();
const PlotHashwhale = new SuperPlot();

window.Driver = {
  initialize: initialize,
  start: driverMain,
  __store: {
    wasOffline: false,
  },
};

function initialize() {

  // Beginning of orientation change
  window.addEventListener('orientationchange', () => {
    Log.debug('Viewport orientation changing.');
    if (View.getCurrentView() === "main") {
      Plotly.purge(document.querySelector('#coincap-plot'));
    } else if (View.getCurrentView() === "hashwhale") {
      Plotly.purge(document.querySelector('#hashwhale-plot'));
    }
  });

  // End of orientation change
  window.addEventListener('orientationchangeend', () => {
    Log.debug('Viewport orientation changed.');
    if (View.getCurrentView() === "main") {
      replotCoincapTrend();
    } else if (View.getCurrentView() === "hashwhale") {
      replotHashwhaleTrend();
    }
  });

  // Viewport resized
  window.addEventListener('resize', () => {
    Log.debug('Viewport resized.');
    if (View.getCurrentView() === "main") {
      replotCoincapTrend();
    } else if (View.getCurrentView() === "hashwhale") {
      replotHashwhaleTrend();
    }
  });

  let devClicks = 0;
  document.querySelector('[view=settings] .logo').addEventListener('click', () => {
    if (devClicks++ === 20) {
      devClicks = 0;
      View.present('devstats');
    }
  }, false);

  // Ads ready
  evee.on('ad-system-ready', () => {
    AdManager.showBannerAd();
    AdManager.ready();
  });

  // Ad presented
  evee.on('ad-present', () => {
    if (View.getCurrentView() === "main") {
      replotCoincapTrend();
    } else if (View.getCurrentView() === "hashwhale") {
      replotHashwhaleTrend();
    }
  });

  // Ad banner presented
  evee.on('ad-banner-success', () => {
    if (View.getCurrentView() === "main") {
      replotCoincapTrend();
    } else if (View.getCurrentView() === "hashwhale") {
      replotHashwhaleTrend();
    }
  });

  // Device is offline
  evee.on('device-offline', () => {
    Driver.__store.wasOffline = true;
    const el = document.querySelector('.banner-offline');
    Animator.show(el);
  });

  // Device is online
  evee.on('device-online', () => {
    Coincap.fetch_page(SYMBOL, fetchCallbackCoincap);
    const el = document.querySelector('.banner-offline');
    Animator.hide(el);
    if (!Driver.__store.wasOffline) return;
    Driver.__store.wasOffline = false;
    AdManager.showBannerAd();
  });

  // Beginning of view change
  evee.on('view-changing', event => {

    // Purge plot
    if (event.data.viewName === "main") {
      Plotly.purge(document.querySelector('#coincap-plot'));
    }

    // Update dev stats if navigating to dev stats
    else if (event.data.viewName === 'devstats') {
      DevStats.updateStats();
    }

    // View fetch whitelist
    const whitelistedCoincap = ["main", "start"];
    const whitelistedHashwhale = ["hashwhale"];

    // Stop coincap fetching if navigating
    // away from the market overview
    if (whitelistedCoincap.includes(event.data.viewFrom)
        && !whitelistedCoincap.includes(event.data.viewName)) {
      Coincap.stop();
    } else if (whitelistedCoincap.includes(event.data.viewName)
        && !whitelistedCoincap.includes(event.data.viewFrom)) {
      Coincap.start();
    }

    // Stop coincap fetching if navigating
    // away from the market overview
    if (whitelistedHashwhale.includes(event.data.viewFrom)
        && !whitelistedHashwhale.includes(event.data.viewName)) {
      Hashwhale.stop();
    } else if (whitelistedHashwhale.includes(event.data.viewName)
        && !whitelistedHashwhale.includes(event.data.viewFrom)) {
      Hashwhale.start();
    }
  });

  // End of view change
  evee.on('view-changed', event => {

    // Replot trend if navigated to market overview
    if (event.data.viewName === "main") {
      replotCoincapTrend();
    } else if (event.data.viewName === "hashwhale") {
      replotHashwhaleTrend();
    }
  });
}

function driverMain() {

  // Setup coincap market data fetching
  Coincap.setup(SYMBOL, FETCH_TIMEOUT_COINCAP, fetchCallbackCoincap, {
    prefetch: true,
    autostart: true,
  });

  // Setup hashwhale pool stats fetching
  Hashwhale.setup(FETCH_TIMEOUT_HASHWHALE, fetchCallbackHashwhale, {
    prefetch: true,
    autostart: false,
  });
}

function fetchCallbackCoincap(data) {
  Coincap.fetch_history(SYMBOL, history => {
    Driver.__store.cachedHistory = history;
    presentCoincapData(data);
    plotCoincapTrend(history);
  });
  if (changed(data)) {
    playAnimation();
  }
}

function fetchCallbackHashwhale(data) {
  Driver.__store.cachedPoolStats = data;
  presentHashwhaleData(data);
  plotHashwhaleTrend(data);
}

function changed(data) {
  const defaultData = {
    supply: undefined,
    volume: undefined,
    price: undefined,
    market_cap: undefined,
    cap24hrChange: undefined,
    price_usd: undefined,
  };
  const lastData = Object.assign({}, defaultData, Driver.__store.lastData);
  const currData = Object.assign({}, defaultData, data);
  return false
    || currData.supply        !== lastData.supply
    || currData.volume        !== lastData.volume
    || currData.price         !== lastData.price
    || currData.market_cap    !== lastData.market_cap
    || currData.cap24hrChange !== lastData.cap24hrChange
    || currData.price_usd     !== lastData.price_usd
    || false;
}

function playAnimation() {
  const logo = document.querySelector('.logo');
  if (logo.classList.contains('animate'))
    logo.classList.remove('animate');
  logo.classList.add('animate');
  setTimeout(() => logo.classList.remove('animate'), 1500);
}

function presentCoincapData(data) {
  for (const price of document.querySelectorAll('.price')) {
    const newPrice = document.createElement('div');
    newPrice.classList.add('price');
    const priceValue = document.createElement('div');
    const priceChange = document.createElement('div');
    priceValue.classList.add('value');
    priceValue.textContent = data.price.toLocaleString();
    priceChange.classList.add('change');
    priceChange.innerHTML = `
      <div class="top">24hr change</div>
      <div class="bottom">${data.cap24hrChange}%</div>
    `.trim();
    if (data.cap24hrChange > 0) {
      priceChange.classList.add('trend--up');
      priceChange.classList.remove('trend--down');
    } else {
      priceChange.classList.add('trend--down');
      priceChange.classList.remove('trend--up');
    }
    newPrice.appendChild(priceValue);
    newPrice.appendChild(priceChange);
    price.replaceWith(newPrice);
  }
  document.querySelector('.overview').innerHTML = `
  <div class="row">
    <span class="label">Market Cap</span>
    <span class="value">$${data.market_cap.toLocaleString()}</span>
  </div>
  <div class="row">
    <span class="label">24h Volume</span>
    <span class="value">$${data.volume.toLocaleString()}</span>
  </div>
  <div class="row">
    <span class="label">Supply</span>
    <span class="value">${data.supply.toLocaleString()}</span>
  </div>
  `.trim();
}

function presentHashwhaleData(data) {
  const ulTarget = document.querySelector('.pool-stats');
  const ul = document.createElement('ul');
  ul.classList.add('pool-stats');
  const props = [
    {
      prop: 'hashrate',
      label: 'Hashrate',
      format: 'hashes',
    },
    {
      prop: 'miners',
      label: 'Miners',
      format: 'number',
    },
    {
      prop: 'totalBlocks',
      label: 'Blocks Found',
      format: 'number',
    },
    {
      prop: 'lastBlockFound',
      label: 'Last Block Found',
      format: 'epoch',
    },
  ];
  for (const propData of props) {
    const dataPoint = data.pool[propData.prop];
    const li = document.createElement('li');
    li.classList.add(propData.class || propData.prop.toLowerCase());
    const label = document.createElement('div');
    label.classList.add('label');
    label.textContent = propData.label;
    const value = document.createElement('div');
    value.classList.add('value');
    value.textContent = (() => {
      switch(propData.format) {
        case 'number': return Number(dataPoint).toLocaleString();
        case 'epoch': return new Date(Number(dataPoint)).toLocaleString();
        case 'hashes': {
          const ctables = [
            [0, "H/sec"],
            [Math.pow(1000, 1), "KH/sec"],
            [Math.pow(1000, 2), "MH/sec"],
            [Math.pow(1000, 3), "GH/sec"],
            [Math.pow(1000, 4), "PH/sec"],
          ];
          for (const [divisor, suffix] of ctables) {
            if (Number(dataPoint) >= divisor) {
              return `${(Number(dataPoint) / (divisor || 1)).toLocaleString()} ${suffix}`;
            }
          }
          return `${dataPoint.toLocaleString()}`;
        }
        default: return String(dataPoint);
      }
    })();
    li.appendChild(label);
    li.appendChild(value);
    ul.appendChild(li);
  }
  ulTarget.replaceWith(ul);
}

function replotCoincapTrend() {
  plotCoincapTrend(Driver.__store.cachedHistory);
}

function replotHashwhaleTrend() {
  plotHashwhaleTrend(Driver.__store.cachedPoolStats);
}

function plotCoincapTrend(history) {

  // Remove the last value (latest)
  history.market_cap.splice(-1, 1);
  history.price.splice(-1, 1);

  // Smooth traces
  history.price = Coincap.smoothOhlcChain(history.price, [5]); // 3, 3, 3, 2
  history.market_cap = Coincap.smoothOhlcChain(history.market_cap, [3]); // 3, 2

  // Construct the plot
  PlotCoincap
    .clear()
    .trace({
      type: 'scatter',
      mode: 'lines',
      name: 'Market Cap',
      x: history.market_cap.map(pair => new Date(pair[0])),
      y: history.market_cap.map(pair => pair[1]),
      line: {
        shape: 'spline',
        smoothing: 0.25,
        width: 3,
        color: getPlotStyle('trace-marketcap', 'color'),
      },
      hoveron: 'points+fills',
      hoverinfo: 'x+y',
      hoverlabel: {
        font: {
          family: getStyle('body', 'font-family'),
        },
      },
      fill: 'tonexty',
      fillcolor: getPlotStyle('trace-marketcap', 'background-color'),
    }).trace({
      type: 'scatter',
      mode: 'lines',
      name: 'Price (USD)',
      x: history.price.map(pair => new Date(pair[0])),
      y: history.price.map(pair => pair[1]),
      yaxis: 'y2',
      line: {
        shape: 'spline',
        smoothing: 0.25,
        width: 3,
        color: getPlotStyle('trace-price', 'color'),
      },
      hoveron: 'points+fills',
      hoverinfo: 'x+y',
      hoverlabel: {
        font: {
          family: getStyle('body', 'font-family'),
        },
      },
      fill: 'tonexty',
      fillcolor: getPlotStyle('trace-price', 'background-color'),
    }).layout({
      showlegend: true,
      dragmode: 'zoom',
      hovermode: 'closest',
      hoverlabel: {
        bgcolor: 'white',
      },
      font: {
        family: getStyle('body', 'font-family'),
        color: getStyle('body', 'color'),
      },
      legend: {
        orientation: 'h',
        xanchor: 'right',
        yanchor: 'bottom',
        x: 0.95,
        y: 0.05,
        bgcolor: getPlotStyle('legend', 'background-color'),
        font: {
          family: getStyle('body', 'font-family'),
          color: getPlotStyle('legend', 'color'),
        }
      },
      margin: {
        l: 0,
        r: 0,
        t: 0,
        b: 0,
        autoexpand: false,
      },
      paper_bgcolor: getStyle('body', 'background-color'),
      plot_bgcolor: getStyle('body', 'background-color'),
      yaxis: {
        visible: false,
        fixedrange: true,
        showgrid: false,
      },
      yaxis2: {
        visible: false,
        fixedrange: true,
        showgrid: false,
        overlaying: 'y',
        side: 'right',
      },
      xaxis: {
        visible: false,
        fixedrange: true,
        showgrid: false,
        type: 'date',
      },
    }).options({
      showLink: false,
      staticPlot: false,
      displayModeBar: false,
      displaylogo: false,
      scrollZoom: false,
      editable: false,
    }).plot('#coincap-plot');
}

function plotHashwhaleTrend(data) {
  PlotHashwhale
    .clear()
    .trace({
      type: 'scatter',
      mode: 'lines',
      name: 'Hashrate',
      x: data.charts.hashrate.map(pair => new Date(pair[0])),
      y: data.charts.hashrate.map(pair => pair[1]),
      xaxis: 'x2',
      line: {
        shape: 'spline',
        smoothing: 0.5,
        width: 3,
        color: getPlotStyle('trace-marketcap', 'color'),
      },
      hoverinfo: 'x+y',
      hoverlabel: {
        font: {
          family: getStyle('body', 'font-family'),
        },
      },
      fill: 'tozeroy',
      fillcolor: getPlotStyle('trace-marketcap', 'background-color'),
    }).trace({
      type: 'scatter',
      mode: 'lines',
      name: 'Difficulty',
      x: data.charts.difficulty.map(pair => new Date(pair[0])),
      y: data.charts.difficulty.map(pair => pair[1]),
      yaxis: 'y2',
      line: {
        shape: 'spline',
        smoothing: 0.5,
        width: 3,
        color: getPlotStyle('trace-price', 'color'),
      },
      hoverinfo: 'x+y',
      hoverlabel: {
        font: {
          family: getStyle('body', 'font-family'),
        },
      },
      fill: 'tozeroy',
      fillcolor: getPlotStyle('trace-price', 'background-color'),
    }).layout({
      showlegend: true,
      dragmode: 'zoom',
      hovermode: 'closest',
      hoverlabel: {
        bgcolor: 'white',
      },
      font: {
        family: getStyle('body', 'font-family'),
        color: getStyle('body', 'color'),
      },
      legend: {
        orientation: 'h',
        xanchor: 'right',
        yanchor: 'bottom',
        x: 0.95,
        y: 0.05,
        bgcolor: getPlotStyle('legend', 'background-color'),
        font: {
          family: getStyle('body', 'font-family'),
          color: getPlotStyle('legend', 'color'),
        }
      },
      margin: {
        l: 0,
        r: 0,
        t: 0,
        b: 0,
        autoexpand: false,
      },
      paper_bgcolor: getStyle('body', 'background-color'),
      plot_bgcolor: getStyle('body', 'background-color'),
      yaxis: {
        visible: false,
        fixedrange: true,
        showgrid: false,
      },
      yaxis2: {
        visible: false,
        fixedrange: true,
        showgrid: false,
        overlaying: 'y',
      },
      xaxis: {
        visible: false,
        fixedrange: true,
        showgrid: false,
        type: 'date',
      },
      xaxis2: {
        visible: false,
        fixedrange: true,
        showgrid: false,
        overlaying: 'x',
        type: 'date',
      },
    }).options({
      showLink: false,
      staticPlot: false,
      displayModeBar: false,
      displaylogo: false,
      scrollZoom: false,
      editable: false,
    }).plot('#hashwhale-plot');
}

// Utility function to extract styles
function getStyle(sel, prop) {
  const el = document.querySelector(sel);
  let result;
  if (el.currentStyle)
    result = el.currentStyle[prop];
  else if (window.getComputedStyle)
    result = document.defaultView.getComputedStyle(el, null).getPropertyValue(prop);
  return result;
};

// Utility to extract plot styles
function getPlotStyle(mapping, prop) {
  return getStyle(`.plot-color-map > [map=${mapping}]`, prop);
};
