// Touch remapping for interactive plot functionality
window.Touchremap = {

  // Initialization
  initialize: function() {

    // Handle touch events as mouse events
    document.addEventListener('touchenter'  , this.__touchHandler, false);
    document.addEventListener('touchleave'  , this.__touchHandler, false);
    document.addEventListener('touchstart'  , this.__touchHandler, false);
    document.addEventListener('touchmove'   , this.__touchHandler, false);
    document.addEventListener('touchend'    , this.__touchHandler, false);
    document.addEventListener('touchcancel' , this.__touchHandler, false);
  },

  __touchHandler: function(event) {
    let touches = event.changedTouches;
    let first = touches[0];
    // Check for plotly
    if (
      !(first.target instanceof SVGRectElement)
    ) return;
    let type;
    switch(event.type) {
      case "touchenter":
        type = "mouseenter"; break;
      case "touchleave":
        type = "mouseleave"; break;
      case "touchstart":
        type = "mousedown"; break;
      case "touchmove":
        type = "mousemove"; break;
      case "touchend":
        type = "mouseup";   break;
      default:
        return;
    }
    let simulatedEvent = new MouseEvent(type, {
      bubbles: true,
      screenX: first.screenX,
      screenY: first.screenY,
      clientX: first.clientX,
      clientY: first.clientY,
    });
    first.target.dispatchEvent(simulatedEvent);
    if (event.type === "touchstart") {
      // Send extra mousemove event
      // This is apparently needed to activate hover functionality
      simulatedEvent = new MouseEvent('mousemove', {
        bubbles: true,
        screenX: first.screenX,
        screenY: first.screenY,
        clientX: first.clientX,
        clientY: first.clientY,
      });
      first.target.dispatchEvent(simulatedEvent);
    }
    event.preventDefault();
  }
};
