window.Log = {

  // FLush queued messages
  ready: function() {
    if (this.__store.ready) return;
    this.__store.ready = true;
    for (const [level, msg] of this.__store.queued) {
      this.queue(level, msg);
    }
    this.__store.queued = [];
  },

  filter: function(level) {
    this.__store.filter = level;
  },

  // Log a silly message
  silly: function(obj, msg) {
    if (msg === void 0) msg = obj;
    if (obj.NAME) msg = `${obj.NAME}: ${msg}`;
    this.__log(0, 'SILL', msg);
  },

  // Log a debug message
  debug: function(obj, msg) {
    if (msg === void 0) msg = obj;
    if (obj.NAME) msg = `${obj.NAME}: ${msg}`;
    this.__log(1, 'DBGM', msg);
  },

  // Log an informational message
  info: function(obj, msg) {
    if (msg === void 0) msg = obj;
    if (obj.NAME) msg = `${obj.NAME}: ${msg}`;
    this.__log(2, 'INFO', msg);
  },

  // Log a warning
  warn: function(obj, msg) {
    if (msg === void 0) msg = obj;
    if (obj.NAME) msg = `${obj.NAME}: ${msg}`;
    this.__log(3, 'WARN', msg);
  },

  // Log an error
  err: function(obj, msg) {
    if (msg === void 0) msg = obj;
    if (obj.NAME) msg = `${obj.NAME}: ${msg}`;
    this.__log(4, 'ERRM', msg);
  },

  // Log a message
  __log: function(level, type,  msg) {
    this.queue(level, `[${type}] ${msg}`);
  },

  // Queues a message
  queue: function(level, msg) {
    this.__store.history.push(`${level} ${msg}`);
    if (this.__store.ready && level >= this.__store.filter) {
      console.log(msg);
    } else if (!this.__store.ready) {
      this.__store.queued.push([level, msg]);
    }
  },

  // Store
  __store: {
    ready: false,
    filter: 0,
    queued: [],
    history: [],
  },
};
