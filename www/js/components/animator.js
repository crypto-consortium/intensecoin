/*
 * <component name=animator>
 * A simple component for animating elements.
 * </component>
 */

window.Animator = {
  NAME: 'Animator',

  show: function(elem, cb) {
    Log.silly(this, `Showing ${elem.getAttribute('view')||elem.constructor.name} ${elem.getAttribute('class')||''}`.trim());
    this.fadeIn(elem, 0, cb);
  },
  hide: function(elem, cb) {
    Log.silly(this, `Hiding ${elem.getAttribute('view')||elem.constructor.name} ${elem.getAttribute('class')||''}`.trim());
    this.fadeOut(elem, 0, cb);
  },
  fadeIn: function(elem, speed, cb) {
    const originalDisplay = elem.getAttribute('data-anim-display') || 'block';
    if (this.getStyle(elem, 'opacity') === 0.0 && getStyle(elem, 'display') === originalDisplay) return;
    if (speed === 0) {
      elem.style.opacity = 1.0;
      elem.style.display = originalDisplay;
      if (cb) cb();
      return true;
    }
    var opacity = 0.0;
    elem.style.opacity = opacity;
    elem.style.display = originalDisplay;
    function step () {
        opacity += speed;
        if (opacity >= 1.0){
            elem.style.opacity = 1.0;
            if (cb) cb();
            return true;
        }
        elem.style.opacity = opacity;
        requestAnimationFrame(step);
    }
    step();
  },
  fadeOut: function(elem, speed, cb) {
    if (this.getStyle(elem, 'opacity') === 0.0 && getStyle(elem, 'display') === 'none') return;
    elem.setAttribute('data-anim-display', this.getStyle(elem, 'display'));
    if (speed === 0) {
      elem.style.opacity = 0.0;
      elem.style.display = 'none';
      if (cb) cb();
      return true;
    }
    var opacity = 1.0;
    elem.style.opacity = opacity;
    elem.style.display = elem.getAttribute('data-anim-display') || 'block';
    function step () {
        opacity -= speed;
        if (opacity <= 0.0){
            elem.style.opacity = 0.0;
            elem.style.display = 'none';
            if (cb) cb();
            return true;
        }
        elem.style.opacity = opacity;
        requestAnimationFrame(step);
    }
    step();
  },
  getStyle: function(el, prop) {
    let result;
    if (el.currentStyle)
      result = el.currentStyle[prop];
    else if (window.getComputedStyle)
      result = document.defaultView.getComputedStyle(el, null).getPropertyValue(prop);
    return result;
  },
};
