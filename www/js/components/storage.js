/*
 * <component name=animator>
 * A persistent storage component utilizing LocalStorage.
 * Works out of the box.
 * </component>
 */

window.PersistentStorage = {

  // Test whether the specified key exists
  probe: function (name) {
    return this.get_prop(name) !== null;
  },

  // Get property
  get_prop: function (name) {
    return localStorage.getItem(name);
  },

  // Set property
  set_prop: function (name, value) {
    localStorage.setItem(name, value);
  },

  // Get object using JSON deserialization
  get_obj: function (name) {
    return JSON.parse(this.get_prop(name) || '{}');
  },

  // Set object using JSON serialization
  set_obj: function (name, value) {
    this.set_prop(name, JSON.stringify(value || {}));
  },

  // Set default property
  set_prop_default: function (name, value) {
    if (this.probe(name)) return;
    this.set_prop(name, value);
  },

  // Set default object using JSON serialization
  set_obj_default: function (name, value) {
    if (this.probe(name)) return;
    this.set_obj(name, value);
  },

  // Clear persistent storage
  clear: function () {
    localStorage.clear();
  },
};
