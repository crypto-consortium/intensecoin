window.DevStats = {
  initialize: function() {
    for (const el of document.querySelectorAll('[devstat-update]')) {
      el.addEventListener('click', e => {
        this.updateStats();
        e.preventDefault();
      });
    }
  },
  updateStats: function() {
    for (const el of document.querySelectorAll('[devstat]')) {
      const attr = el.getAttribute('devstat');
      // Debug
      if (attr === 'debug') {
        el.textContent = 'N/A';
        if (cordova.plugins.IsDebug) {
          cordova.plugins.IsDebug.getIsDebug(function(isDebug) {
            el.textContent = isDebug;
          }, () => {});
        } else {
          el.textContent = 'N/A (error)';
        }
      }

      // Ad Mode
      else if (attr === 'ads') {
        el.textContent = AdManager.__store.debug ? 'testing' : 'production';
      }
    }
  }
};
