/*
 * <component name=animator>
 * AdMob component with debug-mode support.
 * </component>
 */

window.AdManager = {
  NAME: 'AdManager',

  initialize: function (ids) {
    this.__store.ids = ids;
    document.addEventListener('onAdPresent', () => evee.emit('ad-present'));
    Log.debug(this, 'Ready.');
  },

  // Enable debug mode
  debug: function() {
    this.__store.debug = true;
    Log.info(this, `Ads in testing mode.`);
  },

  // Enable production mode
  production: function() {
    this.__store.debug = false;
    Log.info(this, 'Ads in production mode.');
  },

  // Dispatch queued ads
  ready: function() {
    Log.silly(this, 'Dispatching queued ads.');
    this.__store.ready = true;
    for (let i = 0; i < this.__store.queuedBanner.length; i++) {
      this.showBannerAd(this.__store.queuedBanner.pop());
    }
  },

  // Enable ad dispatch
  enable: function() {
    Log.silly(this, 'Enabled ad dispatch.');
    this.__store.enabled = true;
  },

  // Disable ad dispatch
  disable: function() {
    Log.silly(this, 'Disabled ad dispatch.');
    this.__store.enabled = false;
    this.clearBannerAd();
  },

  // Clear banner ad
  clearBannerAd: function() {
    if (!window.AdMob) return;
    AdMob.removeBanner();
    Log.silly(this, 'Cleared banner ad.');
  },

  // Show banner ad
  showBannerAd: function(options) {
    const mergedOptions = Object.assign(
      {},
      options || {},
      {
        isTesting: this.__store.debug
      }
    );
    if (!this.__store.ready) {
      Log.silly(this, 'Queueing banner ad.');
      this.__store.queuedBanner.push(mergedOptions);
      return;
    } else {
      this.__showBannerAd(mergedOptions);
    }
  },

  // Internal: Show banner ad
  __showBannerAd: function(options) {
    if (!window.AdMob || !this.__store.enabled) return;
    Log.silly(this, 'Showing banner ad.');
    const mergedOptions = Object.assign(
      {
        adId: this.__store.ids.banner,
        position: AdMob.AD_POSITION.BOTTOM_CENTER,
        adSize: 'SMART_BANNER',
        overlap: false,
        offsetTopBar: false,
        adExtras: {
          color_bg: 'FFFFFF',
          color_bg_top: 'FFFFFF',
          color_border: 'FFFFFF',
        },
        isTesting: false,
        autoShow: true,
      },
      options || {}
    );
    AdMob.createBanner(mergedOptions, () => evee.emit('ad-banner-success'));
  },

  // Options
  __store: {
    ids: {},
    ready: false,
    debug: false,
    enabled: true,
    queuedBanner: [],
  },
};
