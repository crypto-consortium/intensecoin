window.Coincap = {
  NAME: 'coincap.io',

  // Fetch coin page
  fetch_page: function(symbol, cb) {
    Log.silly(this, `Fetching ${symbol} page.`);
    const url = `http://coincap.io/page/${symbol}`;
    fetch(url, {cache: 'no-store'})
      .then(resp => resp.json())
      .then(json => cb(json))
      .catch(err => console.log(err));
  },

  // Fetch coin history
  fetch_history: function(symbol, cb) {
    Log.silly(this, `Fetching ${symbol} history.`);
    const url = `https://coincap.io/history/30day/${symbol}`;
    fetch(url, {cache: 'no-store'})
      .then(resp => resp.json())
      .then(json => cb(json))
      .catch(err => console.log(err));
  },

  // Setup fetching
  setup: function(symbol, ms, cb, options) {
    const defaults = {
      prefetch: false,
      autostart: false,
    };
    const mergedOptions = Object.assign({}, defaults, options);
    this.__store.symbol = symbol;
    this.__store.ms = ms;
    this.__store.cb = cb;
    if (mergedOptions.autostart) {
      this.start();
    } else if (mergedOptions.prefetch) {
      Log.silly(this, `Prefetching data.`);
      this.fetch_page(symbol, cb);
    }
  },

  // Start fetching
  start: function() {
    if (this.__store.timeout) return;
    Log.silly(this, `Starting fetch with ${this.__store.ms/1000}s interval.`);
    const [symbol, cb, ms] = [
      this.__store.symbol,
      this.__store.cb,
      this.__store.ms,
    ];
    this.fetch_page(symbol, cb);
    this.__store.timeout = setInterval(() => {
      this.fetch_page(symbol, cb);
    }, ms);
  },

  // Stop fetching
  stop: function() {
    if (!this.__store.timeout) return;
    Log.silly(this, `Stopping fetch.`);
    clearInterval(this.__store.timeout);
    this.__store.timeout = void 0;
  },

  /*
  * Smoothens the graph by removing specific data points from the array.
  *
  * Technique:
  * Iterates over the data points in a linear fashion,
  * maintaining a limited view into the last n data points.
  * The view is then used to produce an OHLC candle.
  * If the current closing price is higher than the last high price,
  * or the current opening price is lower than the last low price,
  * the current price value is appended to the resulting data point array.
  *
  * This method is highly efficient in removing unnecessary data points while
  * keeping important data points intact, and produces amazing results
  * in comparison to other methods such as a biased linear search.
  */
  smoothOhlc: function(arr, viewSize, multiScale, last) {
    let view = [];
    const result = !!multiScale ? [] : [arr[0]];
    let lastOhlc = !!multiScale ? last : [0, 0, 0, 0];
    for (let i = 0; i < arr.length; i++) {
      const elem = arr[i];
      const price = elem[1];
      view.push(price);
      if (view.length < viewSize) {
        continue;
      }
      const ohlc = [view.shift(), Math.max(...view), Math.min(...view), price];
      const [currOpen, currHigh, currLow, currClose] = ohlc;
      const [lastOpen, lastHigh, lastLow, lastClose] = lastOhlc;
      if (currClose > lastHigh || currOpen < lastLow) {
        result.push(elem);
      }
      lastOhlc = ohlc;
    }
    if (result.length === 0 || result[result.length - 1][1] !== arr[arr.length - 1][1]) {
      result.push(arr[arr.length - 1]);
    }
    return !!multiScale ? [result, lastOhlc] : result;
  },

  smoothOhlcChain: function(arr, viewSizes) {
    let result = arr;
    let ohlc = [0, 0, 0, 0];
    for (const viewSize of viewSizes) {
      [result, ohlc] = this.smoothOhlc(result, viewSize, true, ohlc);
    }
    result.unshift(arr[0]);
    return result;
  },

  // Store
  __store: {},
};
