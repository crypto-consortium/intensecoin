class SuperPlot {
  constructor() {
    this.clear();
  }

  clear() {
    this.plotTraces = [];
    this.plotLayout = {};
    this.plotOptions = {};
    return this;
  }

  trace(trace) {
    this.plotTraces.push(trace);
    return this;
  }

  layout(layout) {
    this.plotLayout = layout;
    return this;
  }

  options(options) {
    this.plotOptions = options;
    return this;
  }

  plot(sel) {
    const el = document.querySelector(sel);
    Plotly.newPlot(el, this.plotTraces, this.plotLayout, this.plotOptions);
    return this;
  }
}
