window.Themer = {

  earlyInitialize: function(options) {

    // Write options to store
    this.__store.directory = options.directory || 'css';
    this.__store.defaultTheme = options.defaultTheme || 'default';
    this.__store.minified = options.minified || false;

    // Write default theme to persistent storage
    PersistentStorage.set_prop_default('themer--theme-active', this.__store.defaultTheme);

    // Apply preferred theme
    this.__restore({suppressEvents: true});
  },

  // Initialization
  initialize: function() {

    // Install event handlers
    this.__installEventHandlers();
  },

  load: function() {

    // Restore and apply theme
    this.__restore({forceSendEvents: true});
  },

  switch: function(name) {
    this.__apply(name);
    this.__save(name);
  },

  __apply: function(name, options) {
    const mergedOptions = Object.assign(
      {
        suppressEvents: false,
        forceSendEvents: false,
      },
      options || {}
    );
    const path = this.__resolveThemeName(name);
    const targetLink = document.querySelector('[css-theme]');
    const oldName = targetLink.getAttribute('css-theme');
    if (oldName === '') {
      console.log(`Installing theme: ${path}`);
    } else if (oldName === name) {
      console.log(`Theme already installed: ${name}`);
      if (mergedOptions.forceSendEvents) evee.emit('theme-loaded');
      return;
    } else {
      console.log(`Switching theme: ${oldName} -> ${name}`);
    }
    let newLink = document.createElement('link');
    if (!mergedOptions.suppressEvents) {
      newLink.onload = () => evee.emit('theme-loaded');
    }
    newLink.setAttribute('rel', 'stylesheet');
    newLink.setAttribute('type', 'text/css');
    newLink.setAttribute('href', path);
    newLink.setAttribute('css-theme', name);
    targetLink.replaceWith(newLink);
  },

  __save: function(name) {
    PersistentStorage.set_prop('themer--theme-active', name);
  },

  __restore: function(options) {
    const theme = PersistentStorage.get_prop('themer--theme-active');
    this.__apply(theme, options || {});
  },

  // Gets the path to the theme
  __resolveThemeName: function(name) {
    return `${this.__store.directory}/theme-${(this.__store.minified ? `${name}.min` : name)}.css`;
  },

  __installEventHandlers: function() {
    for (const el of document.querySelectorAll('[theme-to]')) {
      el.addEventListener('click', e => {
        this.switch(el.getAttribute('theme-to'));
        e.preventDefault();
      });
    }
  },

  // Options
  __store: {}
};
