window.Hashwhale = {
  NAME: 'Hashwhale',

  fetch_stats: function(cb) {
    Log.silly(this, 'Fetching pool stats.');
    const url = "http://itns.hashwhale.com:8117/stats";
    fetch(url, {cache: 'no-store'})
      .then(resp => resp.json())
      .then(json => cb(json))
      .catch(err => console.log(err));
  },
  setup: function(ms, cb, options) {
    const defaults = {
      prefetch: false,
      autostart: false,
    };
    const mergedOptions = Object.assign({}, defaults, options);
    this.__store.ms = ms;
    this.__store.cb = cb;
    if (mergedOptions.autostart) {
      this.start();
    } else if (mergedOptions.prefetch) {
      Log.silly(this, `Prefetching data.`);
      this.fetch_stats(cb);
    }
  },
  isRunning: function() {
    return !!this.__store.timeout;
  },
  stop: function() {
    if (!this.__store.timeout) return;
    Log.silly(this, `Stopping fetch.`);
    clearInterval(this.__store.timeout);
    this.__store.timeout = void 0;
  },
  start: function() {
    if (this.__store.timeout) return;
    Log.silly(this, `Hashwhale: Starting fetch with ${this.__store.ms/1000}s interval.`);
    const [cb, ms] = [
      this.__store.cb,
      this.__store.ms,
    ];
    this.fetch_stats(cb);
    this.__store.timeout = setInterval(() => {
      this.fetch_stats(cb);
    }, ms);
  },
  __store: {},
};
